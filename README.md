A simple TODO / shopping list.

![Todora dark](https://yphil.frama.io/mobilohm/img/montages/montage-todora.png)

This is **BETA** code, don't use it quite yet.

## Installation

### Android

See [lastest released APK](https://drive.google.com/drive/folders/1iW1-lt6X2Z3W4J-bNcwB2l2biGPv32nc?usp=drive_link) Android installer.

### Desktop

``` shell
cd todora/www
http-server
xdg-open http://localhost:8080
```
