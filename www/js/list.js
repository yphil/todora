const newItem = document.getElementById('new-item-input');
newItem.addEventListener('click', () => {
    newItem.classList.add('is-success');
    newItem.classList.remove('is-danger');
});

function displayTodoList(todoList) {
    const todoListContainer = document.getElementById('todo-list');
    todoListContainer.innerHTML = ''; // Clear previous content

    todoList.forEach((item) => {
        const tableRow = document.createElement('tr');

        const textCell = document.createElement('td');
        textCell.className = 'item-text-td'; // Adding the class for full width
        textCell.textContent = item.text;

        const buttonCell = document.createElement('td');
        buttonCell.className = 'item-remove-button-td'; // Adding the class for right alignment
        const plusButton = document.createElement('button');
        plusButton.className = 'button is-small';
        plusButton.textContent = 'X';

        buttonCell.appendChild(plusButton);
        tableRow.appendChild(textCell);
        tableRow.appendChild(buttonCell);

        todoListContainer.appendChild(tableRow);

        plusButton.addEventListener('click', () => moveToDoneItems(item.id));
    });
}

function displayDoneList(doneList) {
    const doneListContainer = document.getElementById('done-list');
    doneListContainer.innerHTML = ''; // Clear previous content

    doneList.forEach((item) => {
        const tableRow = document.createElement('tr');

        const textCell = document.createElement('td');
        textCell.className = 'item-text-td'; // Adding the class for full width
        textCell.textContent = item.text;

        const buttonCell = document.createElement('td');
        buttonCell.className = 'item-remove-button-td'; // Adding the class for right alignment
        const addButton = document.createElement('button');
        addButton.className = 'button is-small';
        addButton.textContent = 'X';

        buttonCell.appendChild(addButton);
        tableRow.appendChild(textCell);
        tableRow.appendChild(buttonCell);

        doneListContainer.appendChild(tableRow);

        addButton.addEventListener('click', () => moveBackToTodo(item.id));
    });
}

async function initTodoApp() {
    try {
        await TODO.openDB();
        const todoList = await TODO.getTodoList();
        displayTodoList(todoList);
        const doneList = await TODO.getDoneList();
        displayDoneList(doneList);
    } catch (error) {
        console.error('Error initializing Todo App:', error);
    }
}

// Function to remove a todo item
async function removeTodoItem(id) {
    try {
        await TODO.moveToDone(id); // Move the item to the done list first
        const todoList = await TODO.getTodoList();
        const doneList = await TODO.getDoneList();
        displayTodoList(todoList);
        displayDoneList(doneList);
    } catch (error) {
        console.error('Error moving item to done list:', error);
    }
}

// Function to move a todo item to done list
async function moveToDoneItems(id) {
    try {
        await TODO.moveToDone(id);
        const todoList = await TODO.getTodoList();
        const doneList = await TODO.getDoneList();
        displayTodoList(todoList);
        displayDoneList(doneList);
    } catch (error) {
        console.error('Error moving item to done list:', error);
    }
}

async function moveBackToTodo(id) {
    try {
        await TODO.moveBackToTodo(id);
        const todoList = await TODO.getTodoList();
        const doneList = await TODO.getDoneList();
        displayTodoList(todoList);
        displayDoneList(doneList);
    } catch (error) {
        console.error('Error moving item back to TODO list:', error);
    }
}

async function addTodo(evt) {
    if (newItem.value.length) {
        try {
            await TODO.addTodoItem(newItem.value);
            const todoList = await TODO.getTodoList();
            displayTodoList(todoList);
        } catch (error) {
            console.error('Error adding todo item:', error);
        }
    } else {
        newItem.classList.add('is-danger');
    }
}

function openTab(evt, tabName) {
    // Declare all variables
    let i, tabcontent, tablinks;

    // Get all elements with class='tabcontent' and hide them
    tabcontent = document.getElementsByClassName('content-container');
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = 'none';
    }

    // Get all elements with class='tablinks' and remove the class 'active'
    tablinks = document.getElementsByClassName('tablinks');
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(' active', '');
        tablinks[i].parentNode.classList.remove('is-active');
    }

    // Show the current tab, and add an 'active' class to the button that opened the tab
    document.getElementById(tabName).style.display = 'block';
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(' active', '');
        tablinks[i].parentNode.classList.remove('is-active');
    }
    // evt.currentTarget.className += ' active';
    evt.currentTarget.parentNode.classList.add('is-active');
}

// Initial display of todo and done lists
initTodoApp();
