const TODO = (function () {
    const dbName = 'todoDB';
    const dbVersion = 1;
    let db;
    let pages = [];
    let links = [];

    document.addEventListener("DOMContentLoaded", function(){
        pages = document.querySelectorAll('[data-page]');
        links = document.querySelectorAll('[data-role="link"]');
        //pages[0].className = "active";  - already done in the HTML
        [].forEach.call(links, function(link){
            link.addEventListener("click", navigate);
        });
    });

    function navigate(ev) {

        ev.preventDefault();
        let id = ev.currentTarget.href.split("#")[1];

        console.log('yo: ', id);

        let currentTargetId = ev.currentTarget.id;
        var burger = document.querySelector('.navbar-burger');
        var menu = document.querySelector('#' + burger.dataset.target);

        if (ev.currentTarget.id == 'burger' || ev.currentTarget.id == 'logo') {
            burger.classList.toggle('is-active');
            menu.classList.toggle('is-active');
        }

        [].forEach.call(pages, function(page){

            if (ev.currentTarget.id !== 'logo') {
                console.log('object: ');
                burger.classList.toggle('is-active');
            }

            if (ev.currentTarget.id !== 'burger') {
                if(page.id === id){
                    page.classList.add('active');
                    burger.classList.toggle('is-active');
                    menu.classList.toggle('is-active');
                } else {
                    page.classList.remove('active');
                }

            }


        });

        return false;
    }

    function openDB() {
        return new Promise((resolve, reject) => {
            const openDBRequest = indexedDB.open(dbName, dbVersion);

            openDBRequest.onerror = function (event) {
                console.error('Error opening database:', event.target.error);
                reject(event.target.error);
            };

            openDBRequest.onsuccess = function (event) {
                db = event.target.result;
                resolve();
            };

            openDBRequest.onupgradeneeded = function (event) {
                db = event.target.result;
                if (!db.objectStoreNames.contains('todos')) {
                    db.createObjectStore('todos', { keyPath: 'id', autoIncrement: true });
                }
                if (!db.objectStoreNames.contains('done')) {
                    db.createObjectStore('done', { keyPath: 'id', autoIncrement: true });
                }
            };
        });
    }

    function addTodoItem(item) {
        const transaction = db.transaction(['todos'], 'readwrite');
        const todoStore = transaction.objectStore('todos');

        const newItem = { text: item };
        const addRequest = todoStore.add(newItem);

        return new Promise((resolve, reject) => {
            transaction.oncomplete = function () {
                resolve();
            };

            transaction.onerror = function (event) {
                console.error('Error adding todo item:', event.target.error);
                reject(event.target.error);
            };
        });
    }

    function removeTodoItem(id) {
        const transaction = db.transaction(['todos'], 'readwrite');
        const todoStore = transaction.objectStore('todos');

        const deleteRequest = todoStore.delete(id);

        return new Promise((resolve, reject) => {
            transaction.oncomplete = function () {
                resolve();
            };

            transaction.onerror = function (event) {
                console.error('Error removing todo item:', event.target.error);
                reject(event.target.error);
            };
        });
    }

    function moveToDone(id) {
        return new Promise((resolve, reject) => {
            const transaction = db.transaction(['todos', 'done'], 'readwrite');
            const todoStore = transaction.objectStore('todos');
            const doneStore = transaction.objectStore('done');

            transaction.onerror = function (event) {
                console.error('Error moving item to done list:', event.target.error);
                reject(event.target.error);
            };

            todoStore.get(id).onsuccess = function (event) {
                const item = event.target.result;
                if (item) {
                    const addRequest = doneStore.add(item);
                    addRequest.onsuccess = function (event) {
                        todoStore.delete(id);
                        console.log('Item moved to done:', item);
                        resolve();
                    };
                    addRequest.onerror = function (event) {
                        console.error('Error moving item to done list:', event.target.error);
                        reject(event.target.error);
                    };
                } else {
                    console.error(`Todo item with id ${id} not found.`);
                    reject(new Error(`Todo item with id ${id} not found.`));
                }
            };
        });
    }

    // Add the moveBackToTodo method to the TODO object
    function moveBackToTodo (id) {
        return new Promise((resolve, reject) => {
            const transaction = db.transaction(['done', 'todos'], 'readwrite');
            const doneStore = transaction.objectStore('done');
            const todoStore = transaction.objectStore('todos');

            transaction.onerror = function (event) {
                console.error('Error moving item back to TODO list:', event.target.error);
                reject(event.target.error);
            };

            doneStore.get(id).onsuccess = function (event) {
                const item = event.target.result;
                if (item) {
                    const addRequest = todoStore.add(item);
                    addRequest.onsuccess = function (event) {
                        doneStore.delete(id);
                        console.log('Item moved back to TODO:', item);
                        resolve();
                    };
                    addRequest.onerror = function (event) {
                        console.error('Error moving item back to TODO list:', event.target.error);
                        reject(event.target.error);
                    };
                } else {
                    console.error(`Done item with id ${id} not found.`);
                    reject(new Error(`Done item with id ${id} not found.`));
                }
            };
        });
    };

    function getTodoList() {
        return new Promise((resolve, reject) => {
            const transaction = db.transaction(['todos'], 'readonly');
            const todoStore = transaction.objectStore('todos');
            const todoList = [];

            transaction.oncomplete = function () {
                resolve(todoList);
            };

            transaction.onerror = function (event) {
                console.error('Error retrieving todo list:', event.target.error);
                reject(event.target.error);
            };

            todoStore.openCursor().onsuccess = function (event) {
                const cursor = event.target.result;
                if (cursor) {
                    todoList.push({ id: cursor.key, text: cursor.value.text });
                    cursor.continue();
                }
            };
        });
    }

    function getDoneList() {
        return new Promise((resolve, reject) => {
            const transaction = db.transaction(['done'], 'readonly');
            const doneStore = transaction.objectStore('done');
            const doneList = [];

            transaction.oncomplete = function () {
                resolve(doneList);
            };

            transaction.onerror = function (event) {
                console.error('Error retrieving done list:', event.target.error);
                reject(event.target.error);
            };

            doneStore.openCursor().onsuccess = function (event) {
                const cursor = event.target.result;
                if (cursor) {
                    doneList.push({ id: cursor.key, text: cursor.value.text });
                    cursor.continue();
                }
            };
        });
    }

    return {
        openDB,
        addTodoItem,
        removeTodoItem,
        moveToDone,
        getTodoList,
        moveBackToTodo,
        getDoneList,
    };
})();
